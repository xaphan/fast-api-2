up:
	docker compose -f docker-compose.yaml build

down:
	docker compose -f docker-compose.yaml up

migr_gener:
	venv\Scripts\alembic revision --autogenerate

migr_to_base:
	venv\Scripts\alembic upgrade heads
