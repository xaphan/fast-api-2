# from fastapi import FastAPI
from core import get_app_fastapi
import uvicorn

from logger_core.config_logger import ConfigLogger

from example_simple.router_ex_simple import ex_simple_route


logFC = ConfigLogger.getLogger("FileStdout", "main")


app = get_app_fastapi()  # FastAPI()
# app = FastAPI()


app.include_router(ex_simple_route)


if __name__ == "__main__":
    ConfigLogger.settingLogger()

    logFC.info(f"'Start' FastApi = {app}")
    uvicorn.run(app, host="0.0.0.0", port=8000)
    logFC.info(f"'Stop' FastApi = {app}\n'****************************'\n\n")
